       IDENTIFICATION DIVISION. 
       PROGRAM-ID. SQUARE-STAR.
       AUTHOR. NAPHAT.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  SCR-LINE PIC   X(80) VALUE SPACE.
       01  STAR-NUM PIC   9(3)  VALUE 0.

       PROCEDURE DIVISION.
       000-BEGIN.
           PERFORM 002-INPUT-STAR-NUM THRU 002-EXIT
           PERFORM 001-PRINT-STAR-LINE THRU 001-EXIT STAR-NUM TIMES 
           GOBACK 
       .

       001-PRINT-STAR-LINE.
           MOVE ALL "*" TO SCR-LINE(1:10)
           DISPLAY SCR-LINE 
       .
       001-EXIT.
           EXIT 
       .
       002-INPUT-STAR-NUM.
           PERFORM UNTIL STAR-NUM > 0
              DISPLAY "Input star number: " WITH NO ADVANCING 
              ACCEPT STAR-NUM
              IF STAR-NUM = 0 DISPLAY "Please input star > 0"
           END-PERFORM
       .
       002-EXIT.
           EXIT 
       .
      
