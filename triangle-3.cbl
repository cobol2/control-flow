       IDENTIFICATION DIVISION. 
       PROGRAM-ID. TRIANGLE-3.
       AUTHOR. NAPHAT.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  SCR-LINE       PIC X(80) VALUE SPACES .
       01  STAR-NUM       PIC 9(3) VALUE ZEROS .
           88 VALID-STAR-NUM VALUE 1 THRU 80.
       01  INDEX-NUM      PIC 9(3) VALUE ZEROS .

       PROCEDURE DIVISION .
       000-Begin.
           Perform 002-INPUT-STAR-NUM THRU 002-EXIT
           Perform 001-PRINT-STAR-LINE THRU 001-EXIT
              VARYING INDEX-NUM FROM STAR-NUM by -1 
              UNTIL INDEX-NUM = 0
           GOBACK .

       001-PRINT-STAR-LINE.
           MOVE ALL SPACES TO SCR-LINE 
           MOVE ALL "*" TO SCR-LINE(1:INDEX-NUM )
           DISPLAY SCR-LINE 
           .
       001-EXIT.
           EXIT.

       002-INPUT-STAR-NUM.
           PERFORM UNTIL VALID-STAR-NUM 
              DISPLAY "Please input star number: " WITH NO ADVANCING 
              ACCEPT STAR-NUM 
              IF NOT VALID-STAR-NUM 
              DISPLAY "Please input star number in positive number"  
              END-IF
           END-PERFORM
           . 
       002-EXIT.
           EXIT 
       .
