       IDENTIFICATION DIVISION. 
       PROGRAM-ID. TRIANGLE-2.
       AUTHOR. NAPHAT.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  SCR-LINE       PIC X(80) VALUE SPACES .
       01  STAR-NUM       PIC 9(3) VALUE ZEROS .
           88 VALID-STAR-NUM VALUE 1 THRU 80.
       01  INDEX-NUM      PIC 9(3) VALUE ZEROS .
       01  INDEX-NUM2     PIC 9(3) VALUE ZEROS .

       PROCEDURE DIVISION .
       000-BEGIN.
           PERFORM 001-INPUT-STAR-NUM THRU 001-EXIT
           PERFORM VARYING INDEX-NUM FROM 1 BY 1
              UNTIL INDEX-NUM > STAR-NUM
              COMPUTE  INDEX-NUM2 = STAR-NUM - INDEX-NUM + 1
              MOVE ALL "*" TO SCR-LINE(INDEX-NUM2:INDEX-NUM)
              DISPLAY SCR-LINE 
           END-PERFORM
           GOBACK 
           .
       001-INPUT-STAR-NUM.
           PERFORM UNTIL VALID-STAR-NUM 
              DISPLAY "Please input star number: " WITH NO ADVANCING 
              ACCEPT STAR-NUM 
              IF NOT VALID-STAR-NUM 
              DISPLAY "Please input > 1 <= 80"  
              END-IF
           END-PERFORM
           . 
       001-EXIT.
           EXIT 
           .
