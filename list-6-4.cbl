       IDENTIFICATION DIVISION. 
       PROGRAM-ID. LISTING6-4.
       AUTHOR. NAPHAT.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  COUNTERS.
           05 HUNDERDS-COUNT PIC 99 VALUE ZEROS.
           05 TENS-COUNT PIC 99 VALUE ZEROS.
           05 UNITS-COUNT PIC 99 VALUE ZEROS.
       01  ODEMETER.
           05 PRN-HUNDREDS PIC 9.
           05 FILLER PIC X VALUE "-".
           05 PRN-TENS PIC 9.
           05 FILLER PIC X VALUE "-".
           05 PRN-UNITS PIC 9.

       PROCEDURE DIVISION.
       000-BEGIN.
           DISPLAY "Using an out-of-line Perfrom"
           PERFORM 001-CONT-MILEAGE THRU 001-EXIT
              VARYING  HUNDERDS-COUNT FROM 0 BY 1
                 UNTIL HUNDERDS-COUNT > 9
               AFTER TENS-COUNT FROM 0 BY 1 UNTIL TENS-COUNT > 9
               AFTER UNITS-COUNT FROM 0 BY 1 UNTIL UNITS-COUNT > 9

      *    PERFORM VARYING HUNDERDS-COUNT FROM 0 BY 1
      *          UNTIL HUNDERDS-COUNT > 9
      *        PERFORM VARYING TENS-COUNT FROM 0 BY 1 
      *          UNTIL TENS-COUNT > 9
      *          PERFORM VARYING UNITS-COUNT FROM 0 BY 1 
      *             UNTIL UNITS-COUNT > 9
      *             MOVE HUNDERDS-COUNT TO PRN-HUNDREDS
      *             MOVE TENS-COUNT TO PRN-TENS 
      *             MOVE UNITS-COUNT TO PRN-UNITS 
      *             DISPLAY "Out - " ODEMETER
      *    END-PERFORM
      *    END-PERFORM
      *    END-PERFORM
           GOBACK
       .

       001-CONT-MILEAGE.
           MOVE HUNDERDS-COUNT TO PRN-HUNDREDS
           MOVE TENS-COUNT TO PRN-TENS 
           MOVE UNITS-COUNT TO PRN-UNITS 
           DISPLAY "Out - " ODEMETER 
       .
       001-EXIT.
           EXIT
       .
           