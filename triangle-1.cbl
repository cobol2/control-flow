       IDENTIFICATION DIVISION. 
       PROGRAM-ID. TRIANGLE-1.
       AUTHOR. NAPHAT.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  SCR-LINE PIC   X(80) VALUE SPACE.
       01  STAR-NUM PIC   9(3)  VALUE ZEROES.
           88 INVALID-STAR-NUM VALUE 0 THRU 80.
       01  INDEX-NUM PIC   9(3)  VALUE ZEROES.

       PROCEDURE DIVISION.
       000-BEGIN.
           PERFORM 002-INPUT-STAR-NUM THRU 002-EXIT 
           PERFORM 001-PRINT-STAR-LINE THRU 001-EXIT
              VARYING INDEX-NUM FROM 1 BY 1
              UNTIL INDEX-NUM > STAR-NUM 
           GOBACK 
       .

       001-PRINT-STAR-LINE.
           MOVE ALL "*" TO SCR-LINE(1:INDEX-NUM)
           DISPLAY SCR-LINE 
       .
       001-EXIT.
           EXIT 
       .
       002-INPUT-STAR-NUM.
           PERFORM UNTIL STAR-NUM > 0 AND STAR-NUM <= 80
              DISPLAY "Input star number: " WITH NO ADVANCING 
              ACCEPT STAR-NUM
              IF NOT INVALID-STAR-NUM DISPLAY "Please input > 0 <= 80"
           END-PERFORM
       .
       002-EXIT.
           EXIT 
       .
      
